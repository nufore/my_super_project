from pydantic import BaseModel, ConfigDict


class BaseRecipe(BaseModel):
    name: str
    cooking_time: float
    ingredients: str
    description: str


class RecipeIn(BaseRecipe):
    pass


class RecipeOut(BaseRecipe):
    id: int
    model_config = ConfigDict(from_attributes=True)


class RecipesForList(BaseModel):
    name: str
    views: int
    cooking_time: float
